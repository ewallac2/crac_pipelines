# CRAC_Pipelines
This is a repository contaning the scripts for the single-end (SE) and paired-end (PE) pipelines.
This requires you to install:

ruffus
pyCRAC
flexbar
novoalign

## Test/vignette script

There is a 1-step test.vignette pipeline in file `test_pipeline_flexbar_SE.py`. 
All this does is run flexbar on the input file. It thus tests that you have ruffus and flexbar installed.
To run the vignette, from the `crac_pipelines` directory run:

```
python test_pipeline_flexbar_SE.py \
  -f data/20181101_Ssd1_CRAC_init10000.fastq \
  -p 4 \
  -a data/3primeadapter.fasta 
```

This pipeline run will create files:

* `.ruffus_history.sqlite`
* `flexbarOut.log`
* `test_flexbar_ddmmyyy/` - where ddmmyyyy indicates date of the run

Please delete those before running another test, and do not commit them to the repository.
