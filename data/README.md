# crac_pipelines/data

This folder contains sample data to test for running CRAC pipelines


## 20181101_Ssd1_CRAC_init10000.fastq

FASTQ file of initial 10000 reads only, from CRAC experiment performed by Rosey Bayne and Stefan Bresson, measuring Ssd1 binding in S. cerevisiae yeast.

## 3primeadapter.fasta

FASTA file with Illumina small RNA 3' adapter (TGGAATTCTCGGGTGCCAAGGC), used for library construction in 20181101_Ssd1_CRAC_init10000.fastq

## Ssd1_Barcodes.txt

Tab-delimited text file with barcodes for the Ssd1 libraries in 20181101_Ssd1_CRAC_init10000.fastq
