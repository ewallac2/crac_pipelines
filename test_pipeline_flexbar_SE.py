#!/usr/bin/python
__author__		= "Sander Granneman"
__copyright__	= "Copyright 2019"
__version__		= "0.6.1"
__credits__		= ["Sander Granneman", "Edward Wallace"]
__email__		= "sgrannem@staffmail.ed.ac.uk"
__status__		= "beta"

from ruffus import *
from ruffus.cmdline import MESSAGE
from collections import defaultdict
import ruffus.cmdline as cmdline
import subprocess
import platform
import glob
import re
import os
import argparse
import sys
import time

parser = cmdline.get_argparse(description="test pipeline that runs flexbar only")
parser.add_argument("-f",dest="forwardreads",help="the path to your fastq read files.",metavar="data_1.fastq data_2.fastq ...",nargs="*",default=None)
parser.add_argument("--name",dest="name",help="provide a single word describing the run. Default is 'test_flexbar' with a time stamp",default="test_flexbar_%s" % time.strftime("%d%m%Y"))
parser.add_argument("-a","--adapterfile",dest="adapterfile",help="file containing the 3' adapter sequences for trimming the reads using flexbar. If you do not provide adapter file or preset, the trimming step will be skipped",default=None)
parser.add_argument("-aa","--adapterpreset",dest="adapterpreset",help="adapter preset string [TruSeq, SmallRNA, Methyl, Ribo, Nextera, and NexteraMP]. Requires Flexbar version 3.4.0 or later.",default=None)
parser.add_argument("-p","--processors",dest="processors",type=int,help="indicate how many processors you want to use for analyses. Default is 8",default=8)
args = parser.parse_args()

def getFileBaseName(filename):
	""" removes path and extension from file name """
	return os.path.splitext(os.path.basename(filename))[0]

def getBarcodeInfo(barcodefile):
	return ["%s.fastq" % "_".join(line.strip().split()) for line in open(barcodefile,"r").readlines()]
	
def runFlexBar(inputfile,outputfile):
	""" runs Flexbar on the data to remove the adapter sequence from the forward reads """
	if args.adapterfile:
		cmd = "flexbar -r '%s' -qf i1.8 --output-reads '%s' -n %i -ao 7 --adapters '%s' -qt 30" % (inputfile,outputfile,args.processors,args.adapterfile)
	elif args.adapterpreset:
		cmd = "flexbar -r '%s' -qf i1.8 --output-reads '%s' -n %i -ao 7 -aa %s -qt 30" % (inputfile,outputfile,args.processors,args.adapterpreset)
	else:
		cmd = "flexbar -r '%s' -qf i1.8 --output-reads '%s' -n %i -ao 7 -qt 30" % (inputfile,outputfile,args.processors)
	logger.info(cmd)
	os.system(cmd)

### checking if dependencies are installed

dependencies = ["flexbar"]

for i in dependencies:
	cmd = "where" if platform.system() == "Windows" else "which"
	try: 
		subprocess.call([cmd, "%s"])
	except: 
		sys.stderr.write("%s is not installed (properly) on your system. Please (re)install" % i)
		exit()
		
### setup of pipeline

# setting the main working directory
home_dir = os.getcwd()
root_dir = "%s/%s" % (home_dir,args.name)
try:
	os.mkdir(root_dir)
except OSError:
	pass

### setting up logging
if not args.log_file:
	logfile = "%s/%s" % (root_dir,"log_file.txt")
else:
	logfile = "%s/%s" % (root_dir,args.log_file)
logger, logger_mutex = cmdline.setup_logging ("CRAC_Pipeline_SE",logfile,10)

### setting the starting files

startingfiles = [os.path.abspath(i) for i in args.forwardreads]

logger.info("analysing the following files:\n%s\n" % "\n".join(startingfiles))

### start of pipeline

pipeline = Pipeline(name="Test pipeline, single-end data flexbar")
				
if args.adapterfile or args.adapterpreset:
	logger.info("trimming requested")
	pipeline.transform(task_func = runFlexBar,
						input  = startingfiles,
						filter = formatter("^.+/([^/]+).(san)?fastq"),						
						output = "%s/flexbar_trimmed/{1[0]}_trimmed.fastq" % root_dir,
					).follows(pipeline.mkdir(os.path.join(root_dir,"flexbar_trimmed")))
		
elif not args.adapter:
	logger.info("No trimming requested")

### print out flowchart describing pipeline
pipeline_printout_graph("%s/flowchart.jpg" %root_dir)

### run the pipeline
pipeline_run(multiprocess=args.processors,verbose=5,checksum_level=3)
